const express = require('express');
// const mongoose = require('mongoose');
const passport = require('passport');
const bodyParser = require('body-parser');
const cors = require('cors');
const morgan = require('morgan');
const path = require('path');
const authRoutes = require('./routes/auth');
const tasksRoutes = require('./routes/tasks');
// const keys = require('./config/keys.dev.js')
const app = express();

// mongoose.connect(keys.mongoURI, {
//   useNewUrlParser: true,
//   useCreateIndex: true
// })
//   .then(() => console.log('Mongo DB connected'))
//   .catch(error => console.log(error))

app.use(passport.initialize());
require('./middleware/passport')(passport);

app.use(morgan('dev'));
app.use(express.static('client/public'));
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(cors());

app.use('/api/auth', authRoutes);
app.use('/api/tasks', tasksRoutes);

if (process.env.NODE_ENV === 'production') {
  // app.use(express.static('dist'))
  app.get('*', (req, res) => {
    res.sendFile(
      path.resolve(
        __dirname, 'client', 'public', 'index.html'
      )
    )
  })
}

module.exports = app;