const axios = require('axios');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
// const User = require('../models/Users')
const keys = require('../config/keys');
const errorHandler = require('../utils/errorHandler');

module.exports.login = async function (req, res) {
  // const candidate = await User.findOne({email: req.body.email})
  let candidate = await axios.get(`http://localhost:3000/users?login=${req.body.login}`);
  candidate = candidate.data[0];

  console.log(req.body)
  if (!!candidate) {
    const passwordResult = bcrypt.compareSync(req.body.password, candidate.password);
    if (passwordResult) { // генерируем токен, если пароли совпали
      const token = jwt.sign({
        login: candidate.login,
        userId: candidate.id
      }, keys.jwt, {expiresIn: 3600});
      res.status(200).json({
        accessToken: `Bearer ${token}`,
        user: candidate
      });
    } else {
      res.status(401).json({
        message: 'Неправильно указан пароль'
      })
    }
  } else {
    res.status(404).json({
      message: 'Пользователь с таким именем не найден'
    })
  }
  res.end();
};

module.exports.authCheck = function (req, res) {
  res.status(200).json(req.user);
  res.end();
};

module.exports.register = async function (req, res) {
  try {
    // const candidate = await User.findOne({email: req.body.email})
    let count = await await axios.get(`http://localhost:3000/users`);
    count = Number(count.headers['content-length']);

    let candidate = await axios.get(`http://localhost:3000/users?login=${req.body.login}`);
    candidate = candidate.data[0];

    if (candidate) { // Пользователь уже существует

      res.status(409).json({
        message: 'Такой пользователь уже существует'
      });
      // console.log(candidate)
      res.end();
    } else {
      const salt = bcrypt.genSaltSync(11);
      const password = req.body.password;
      const id = count + 1;
      const user = {
        id: id,
        login: req.body.login,
        name: req.body.name,
        email: req.body.email,
        password: bcrypt.hashSync(password, salt)
      };
      try {
        await axios.post(`http://localhost:3000/users`, user);
      } catch (e) {
        errorHandler(res, e);
      }
      const data = {
        id: id,
        user_id: id,
        tasks: [
          {
            id: 1,
            title: 'Пример',
            descr: 'примерное описание',
            date: {
              start: null,
              end: null
            },
            priority: '',
            contexts: [],
            list: 2
          },
          {
            id: 2,
            title: 'Пример2',
            descr: 'примерное описание',
            date: {
              start: null,
              end: null
            },
            priority: 'low',
            contexts: [0],
            list: 0
          },
          {
            id: 3,
            title: 'Пример из списка Учеба',
            descr: 'примерное описание',
            date: {
              start: null,
              end: null
            },
            priority: 'high',
            contexts: [0, 1],
            list: 1
          },
          {
            id: 4,
            title: 'Еще пример из списка Работа',
            descr: 'примерное описание',
            date: {
              start: null,
              end: null
            },
            priority: 'high',
            contexts: [0],
            list: 2
          }
        ],
        lists: [
          {
            id: 0,
            title: 'Входящие',
            color: '#212223'
          },
          {
            id: 1,
            title: 'Учеба',
            color: '#ff7400'
          },
          {
            id: 2,
            title: 'Работа',
            color: '#12d0ff'
          }
        ],
        contexts: [
          {
            id: 0,
            title: 'дома'
          },
          {
            id: 1,
            title: 'в магазине'
          }
        ]
      };
      try {
        await axios.post(`http://localhost:3000/data`, data);
      } catch(e) {
        errorHandler(res, e);
      }
      const token = jwt.sign({
        login: user.login,
        userId: user.id
      }, keys.jwt, {expiresIn: 3600});
      res.status(201).json({
        accessToken: `Bearer ${token}`,
        user
      });
      res.end();
    }
    // const user = new User({
    //   email: req.body.email,
    //   password: bcrypt.hashSync(password, salt)
    // })
    // try {
    //   await user.save()
    //   res.status(201).json(user)
    //   res.end()
    // } catch (e) {
    //   errorHandler(res, e)
    // }
  } catch (e) {

    console.log('error')
    errorHandler(res, e);
  }
};