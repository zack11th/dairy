const axios = require('axios');
// const Cards = require('../models/Cards')
const errorHandler = require('../utils/errorHandler');

module.exports.getAll = async function (req, res) {
  try {
    let data = await axios.get(`http://localhost:3000/data?user_id=${req.params.id}`);
    data = data.data[0];
    res.status(200).json(data);
    res.end();
  }catch (e) {
    errorHandler(res, e);
  }
};

module.exports.addTask = async function (req, res) {
  try {
    console.log('-------------------------------------------------------------')
    console.log(req.params.id)
    let result = await axios.get(`http://localhost:3000/data?user_id=${req.params.id}`);
    let temp = result.data[0];
    console.log('СТАРЫЙ РЕЗАЛТ')
    console.log(temp.tasks)
    temp.tasks.push(req.body)
    console.log('НОВЫЙ РЕЗАЛТ')
    console.log(temp.tasks)
    let data = await axios.patch(`http://localhost:3000/data/${req.params.id}`, temp);
    // console.log(data.data)
    // res.status(201).json(data);
    // res.end();
  }catch (e) {
    errorHandler(res, e);
  }
};