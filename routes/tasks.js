const express = require('express');
const passport = require('passport');
const controller = require('../controllers/tasks');
const router = express.Router();

// /api/tasks
// router.get('/login', controller.login);
router.get('/:id', passport.authenticate('jwt', {session: false}), controller.getAll);
router.post('/:id', passport.authenticate('jwt', {session: false}), controller.addTask);

module.exports = router;