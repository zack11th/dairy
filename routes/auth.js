const express = require('express');
const passport = require('passport');
const controller = require('../controllers/auth');
const router = express.Router();

// /api/auth
router.post('/login', controller.login);
router.get('/', passport.authenticate('jwt', {session: false}), controller.authCheck);
router.post('/register', controller.register);

module.exports = router;