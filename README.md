# install dependencis

- npm install
- cd client
- npm install


# start all project on dev (frontend on localhost:8080)
- npm run dev

# start only json-server as database (localhost:3000)
- npm run fake

# start only server as backend (localhost:5000)
- npm run server