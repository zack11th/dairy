import axios from 'axios';
import {url} from './api.config'

export default{
  namespaced: true,
  state: {
    loading: false,
    userId: null,
    tasks: [],
    lists: [],
    contexts: []
  },
  mutations: {
    setLoading(state, value) {
      state.loading = value;
    },
    setUserId(state, id) {
      state.userId = id;
    },
    setState(state, data) {
      state.tasks = data.tasks;
      state.lists = data.lists;
      state.contexts = data.contexts;
    },
    addTask(state, task) {
      state.tasks.push(task);
    },
    clearState(state) {
      state.tasks = [];
      state.lists = [];
      state.contexts = [];
    }
  },
  actions: {
    getAll({commit}, id) {
      commit('setLoading', true);
      commit('setUserId', id);
      axios.get(`${url.API}/api/tasks/${id}`)
        .then(response => {
          commit('setState', response.data);
          commit('setLoading', false);
          console.log(response)
        }, error => {
          console.log(error.response.data.message)
          commit('setLoading', false);
        });
    },
    addTask({commit, state}, payload) {
      let arr = [];
      state.tasks.forEach(item => {
        arr.push(item.id)
      });
      let id = Math.max(...arr) + 1;
      let task = {id, ...payload};
      commit('addTask', task);
      axios.post(`${url.API}/api/tasks/${state.userId}`, task)
        .then(responce => {
          console.log(responce)
        }, error => {
          console.log(error.response.data.message)
        });
    }
  },
  getters: {
    loading: (state) => state.loading,
    tasks: (state) => state.tasks,
    lists: (state) => state.lists,
    contexts: (state) => state.contexts,
  }
}