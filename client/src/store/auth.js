import axios from 'axios';
import {url} from './api.config'

function setToken(token) {
  localStorage.setItem('user-token', token);
  axios.defaults.headers.common['Authorization'] = token;
}

export default {
  namespaced: true,
  state: {
    token: localStorage.getItem('user-token') || '',
    user: {},
    loading: false
  },
  mutations: {
    setLoading(state, value) {
      state.loading = value;
    },
    authSuccess(state, {token, user}) {
      state.token = token;
      state.user = user;
    },
    logout(state) {
      state.token = '';
      localStorage.removeItem('user-token');
      delete axios.defaults.headers.common['Authorization'];
    }
  },
  actions: {
    authRequest({commit}, user) {
      return new Promise((resolve, reject) => {
        commit('setLoading', true);
        axios.post(`${url.API}/api/auth/login`, user)
          .then(response => {
            setToken(response.data.accessToken);
            commit('authSuccess', {
              token: response.data.accessToken,
              user: response.data.user
            });
            commit('setLoading', false);
            resolve();
          }, error => {
            commit('setLoading', false);
            console.log(error.response.data.message)
            reject();
          });
      });
    },
    registrationRequest({commit}, user) {
      return new Promise((resolve, reject) => {
        commit('setLoading', true);
        axios.post(`${url.API}/api/auth/register`, user)
          .then(response => {
            setToken(response.data.accessToken);
            commit('authSuccess', {
              token: response.data.accessToken,
              user: response.data.user
            });
            commit('setLoading', false);
            resolve();
          }, error => {
            commit('setLoading', false);
            console.log(error.response.data.message)
            reject();
          });
      });
    },
    authCheck({commit, state}) {
      return new Promise((resolve, reject) => {
        commit('setLoading', true);
        axios.get(`${url.API}/api/auth/`)
          .then(response => {
            commit('authSuccess', {
              token: state.token,
              user: response.data
            });
            commit('setLoading', false);
            resolve(response.data.id);
          }, error => {
            commit('setLoading', false);
            commit('logout');
            console.log('Вы не авторизованы')
            reject();
          })
      })
    }
    // logout({commit}) {
    //   return new Promise((resolve, reject) => {
    //     commit('logout');
    //     localStorage.removeItem('user-token');
    //     delete axios.defaults.headers.common['Authorization'];
    //     resolve();
    //   });
    // }
  },
  getters: {
    loading(state) {
      return state.loading;
    },
    isAuthenticated (state) {
      return !!state.token;
    },
    user(state) {
      return state.user;
    }
  }
}

