import Vue from 'vue'
import Vuex from 'vuex'
import base from './base'
import auth from './auth'

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    base, auth
  },
  strict: process.env.NODE_ENV !== 'production'
});
